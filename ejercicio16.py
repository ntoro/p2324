class Libro:
    def __init__(self, titulo, autor, paginas, calificacion):
        self.titulo = titulo
        self.autor = autor
        self.paginas = paginas
        self.calificacion = calificacion
     
    def __str__(self):
        print('Título: '+ self.titulo + ' , autor: ' + self.autor + ', páginas: ' + self.paginas + ' y calificación: ' + self.calificacion)

class ConjuntoLibro:
    def __init__(self, capacidad):
        self.libros = []
        self.capacidad = capacidad

    def listaLibros(self, libro):
        if len(self.libros) < self.capacidad:
            self.libros.append(libro)
            print(self.libros)
        else:
            print('No se pueden añadir más libros')
    
    def añadirLibros(self, libro):
        if libro not in self.libros:
            self.libros.append(libro)
        else:
            print('Este libro ya está en la lista')

    def eliminarLibros(self, titulo):
        for libro in self.libros:
            if libro.titulo == titulo:
                self.libros.remove(libro)
                print('El libro ' + self.titulo + ' ha sido eliminado')

    def ordenarPuntuacion(self):
        lista_calificaciones = []
        for libros in self.libros:
            calificacion = int(input('Introduzca la calificación del libro ' + str(self.libros) + ' : ' ))
            lista_calificaciones.append(calificacion)
        lista_ordenada = lista_calificaciones.sort
    
libro01 = Libro('Verdad', 'Care Santos', 300, 7)
libro02 = Libro('Orgullo y prejuicio', 'Jane Austen', 400, 9)
libro03 = Libro('Daisy Jones', 'Taylor', 450, 9.5)

lista_libros = ConjuntoLibro(3)
lista_libros.listaLibros(libro01)
lista_libros.listaLibros(libro02)
lista_libros.listaLibros(libro03)


print(lista_libros.ordenarPuntuacion())


