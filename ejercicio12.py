class Tiempo:
    def __init__(self, h = 0, min = 0, s = 0):
        self.horas = h
        self.minutos = min
        self.segundos = s

    def setHora(self, hora):
        if self.horas >= 0 and self.horas <= 24:
            self.horas = hora 
        else:   
            print('ERROR: Las horas debe estar entre 1 y 24')
    
    def setMin(self, minuto):
        if self.minutos >=0 and self.minutos < 60:
            self.minutos = minuto
        else:
            print('ERROR: Los minutos deben estar entre 1 y 60')
    def setSeg(self, segundo):
        if self.segundos >= 0 and self.segundos <60:
            self.segundos = segundo
        else:
            print('ERROR: Los segundos deben estar entre 1 y 60')

    def __str__(self, hora, minuto, segundo):
        print('Las horas son ' + hora + ' ,los minutos son ' + minuto + ' y los segundos son ' + segundo)

tiempo01 = Tiempo(12, 85, 2)
tiempo01.setHora(12)
tiempo01.setMin(85)
tiempo01.setSeg(2)