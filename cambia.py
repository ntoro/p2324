def cambia(a,b):
 tmp = a.propietario
 a.propietario = b.propietario
 b.propietario = tmp


class Vehiculo:
    def __init__(self, p):
        self.propietario = p

p1 = Vehiculo('pepe')
# p1.propietario = 'Luis' #cambia el valor de propietario


p2 = Vehiculo('ana') 

#p1 = p2 #con esto p1 y p2 cogen el mismo valor

cambia(p1, p2)
print('p1 = ', p1.propietario)
print('p2 = ', p2.propietario)