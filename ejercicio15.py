class Cuenta:
    def __init__(self, n_cuenta, saldo):
        self.n_cuenta = n_cuenta
        self.saldo = saldo

    def getSaldo(self):
        return self.saldo
    
    def recibirDinero(self, dinero):
        self.saldo = self.saldo + dinero
        return self.saldo
    
    def transferirDinero(self, dinero):
        self.saldo = self.saldo - dinero
        return self.saldo

class Persona(Cuenta):
    def __init__(self, n_cuenta, saldo, maximo, dni):
        super().__init__()
        self.maximo = maximo
        self.dni = dni 

    def numeroCuentas(self, cuentas):
        saldos = []
        if cuentas > 3:
            print('Ha alcanzado el máximo número de cuentas para crear.')


    def deudas(self):
        if self.saldo < 0:
            print('El saldo de esta cuenta es negativo.')
