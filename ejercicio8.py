class DNI:
    def __init__(self, numero):
        self.numero = numero
        '''self.letra = letra'''

    def calcularLetra(self):
        resto = self.numero % 23
        if resto == 0:
            self.letra = 'T'
        elif resto == 1:
            self.letra = 'R'
        elif resto == 2:
            self.letra = 'W'
        elif resto == 3:
            self.letra = 'A'
        elif resto == 4:
            self.letra = 'G'
        elif resto == 5:
            self.letra = 'M'
        elif resto == 6:
            self.letra = 'Y'
        elif resto == 7:
            self.letra = 'F'
        elif resto == 8:
            self.letra = 'P'
        elif resto == 9:
            self.letra = 'D'
        elif resto == 10:
            self.letra = 'X'
        elif resto == 11:
            self.letra = 'B'
        elif resto == 12:
            self.letra = 'N'
        elif resto == 13:
            self.letra = 'J'
        elif resto == 14:
            self.letra = 'Z'
        elif resto == 15:
            self.letra = 'S'
        elif resto == 16:
            self.letra = 'O'
        elif resto == 17:
            self.letra = 'V'
        elif resto == 18:
            self.letra = 'H'
        elif resto == 19:
            self.letra = 'L'
        elif resto == 20:
            self.letra = 'C'
        elif resto == 21:
            self.letra = 'K'
        elif resto == 22:
            self.letra = 'E'
        else:
            print('ERROR: vuelva a introducir el número')

        print(self.letra)

numero1 = DNI(50489490)
numero1.calcularLetra()