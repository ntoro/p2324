from math import pi

class Area():
    def calculaArea(self):
        pass

class Triangulo(Area):
    def __init__(self, base, altura):
        self.base = base
        self.altura = altura

    def calculaArea(self):
        return (self.altura * self.base)/ 2

class Circulo(Area):
    def __init__(self, radio):
        self.radio = radio 

    def calculaArea(self):
        return pi * (self.radio)** 2
    


triangulo1 = Triangulo(10, 8)
print(triangulo1.calculaArea())

circulo1 = Circulo(4)
print(circulo1.calculaArea())