class Alumno:
    def __init__(self, nombre, matricula, nota1, nota2, nota3):
        self.nombre = nombre
        self.matricula = matricula
        self.nota1 = nota1
        self.nota2 = nota2
        self.nota3 = nota3

    def comprobacion(self):
        nota_media = (self.nota1 + self.nota2 + self.nota3) / 3 
        if nota_media < 4.8:
            print('El alumno: ' + self.nombre + ' no ha aprobado.')
        else:
            print('El alumno: ' + self.nombre + ' ha aprobado.')

alumno01 = Alumno('Jorge', '52496', 5, 2.5, 6)
print('Matrícula del alumno: ' + alumno01.matricula)
alumno01.comprobacion()
