class Coche:
    def __init__(self, color, marca, modelo, caballos, puertas, matricula):
        self.color = color
        self.marca = marca
        self.modelo = modelo
        self.caballos = caballos
        self.puertas = puertas
        self.matricula = matricula
    
    def cocheFamiliaNumerosa(self):
        if self.puertas < 5:
            print('El coche no es óptimo para una familia numerosa')
        elif self.modelo == 'Fiat 500':
            print('El coche no es óptimo para una familia numerosa')
        else:
            print('El coche es óptimo para una familia numerosa')
