import random
cifras = 6


def generar_num_aleatorio (num_cifras):
    lista_cifras = []
    while (len(lista_cifras) < num_cifras):
        cifra_aleatoria = random.randrange(10)
        if cifra_aleatoria not in lista_cifras:
            lista_cifras.append(cifra_aleatoria)
            
    return lista_cifras


def convertir_lista_a_numero(lista_cifras):
    numero = 0
    numero_cifras = len(lista_cifras)
    for index in range (numero_cifras):
        numero += lista_cifras[index] * (10 ** (numero_cifras -1 - index))
    
    
    return numero 

lista = generar_num_aleatorio(cifras)
num = convertir_lista_a_numero(lista)
print (lista, num) 
