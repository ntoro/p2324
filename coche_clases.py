class Coche:
    def __init__(self, color, marca, modelo, matricula, velocidad):
        self.color = color
        self.marca =  marca
        self.modelo = modelo
        self.matricula = matricula
        self.velocidad = velocidad


    def aceleracion(self):
        return self.velocidad * 10

    def freno(self):
        return self.velocidad/10

    def __str__(self):
        return('La aceleracion del coche es {aceleracion} m/s**2 y el freno es {freno} m/s**2                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  '.format(aceleracion = self.aceleracion(), freno= self.freno()))

coche1 = Coche('negro', 'Mercedes', 'GLA', '0246 INP', 40)
print(coche1)

