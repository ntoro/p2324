class Cuadrado:
    def __init__(self, l):
         self.lado = l

    def area(self):
        return self.lado * self.lado

    def perimetro(self):
        return 4 * self.lado

    def __str__(self):
        return

cuadrado01 = Cuadrado(2)
cuadrado02 = Cuadrado(3) 
print(cuadrado01.perimetro())
print(cuadrado02.perimetro())
print(cuadrado01.area())
print(cuadrado02.area())
