import json

person_json = '[{"nombre": "Jimena", "cuenta": "jmena"}, {"nombre": Natalia, "cuenta": "ntoro"}]'

person_dict = json.loads(person_json)

print('Nombres:')
for person in person_dict:
    print(person["nombre"])

num_personas = len(person_dict)
print("\nNúmero de personas que tienen cuenta:", num_personas)
