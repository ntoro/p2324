from http.server import HTTPServer, BaseHTTPRequestHandler
from urllib.parse import urlparse

HOST_ADDRESS = "212.128.254.143"
HOST_PORT = 8080

formhtml = """
<!DOCTYPE html>
<html>
<body>

<h2>HTML Forms</h2>

<form action="/damerespuesta">
  <label for="fname">Dame el alimento:</label><br>
  <input type="text" id="alimento" name="fname" value="John"><br>
  <label for="lname">Dame el nutriente:</label><br>
  <input type="text" id="nutriente" name="lname" value="Doe"><br><br>
  <input type="submit" value="Submit">
</form>

<p>If you click the "Submit" button, the form-data will be sent to a page called "/action_page".</p>

</body>
</html>"""

responsehtml = """
<!DOCTYPE html>
<html>
<body>

<h2>HTML Forms</h2>

<p> Hola Amigos!</p>

</body>
</html>"""

class RequestHandler(BaseHTTPRequestHandler):
    def set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self.set_response()
        if (self.path =="/" or self.path == "/index.html"):
            self.wfile.write(formhtml.encode('utf-8'))
        elif (self.path.startswith("/action_page")):
            query = urlparse(self.path).query
            query_components = dict(qc.split("=") for qc in query.split("&"))#a partir de la interrogacion (en el navegador)query aparacen los nombres que yo quiero. Esta linea lo esta troceando y me da un dicionario
            fname = query_components["fname"]
            lname = query_components["lname"]               #recoger las excepciones para evitar que el servidor se rompa
            print(fname, lname)
            self.wfile.write(responsehtml.encode('utf-8'))

    def do_POST(self):
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data#en el post no aparecen mi contenido en la URL esta oculto
        post_data = self.rfile.read(content_length) # <--- Gets the data itself

        self.set_response()
        self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))
        self.wfile.write("POST data is {}".format(post_data ).encode('utf-8'))

def run(server_class=HTTPServer, handler_class=BaseHTTPRequestHandler):
    """ follows example shown on docs.python.org """
    server_address = (HOST_ADDRESS, HOST_PORT)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()

if __name__ == '__main__':
    run(handler_class=RequestHandler)
