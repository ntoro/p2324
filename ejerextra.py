class Persona():
    def __init__(self, DNI, n, a):
        self.DNI = DNI
        self.nombre = n
        self.apellido = a

    def __str__(self):
        return f'La persona con DNI {self.DNI} se llama {self.nombre} {self.apellido}'
    
class Alumno(Persona):
    def __init__(self, DNI, n, a, padron):
        self.padron = padron
        super().__init__(DNI, n, a)
    def __str__(self):
        return f'La persona con DNI {self.DNI} se llama {self.nombre} {self.apellido} con padrón: {self.padron}'
    

alumno01= Alumno('50489490M', 'Natalia', 'Toro Garvia', '02153')
print(alumno01)   