class Fraccion:
    def __init__(self, numerador= 0.0, denominador=1.0):
        self.numerador = numerador
        self.denominador = denominador

    def sumar(izq,dcha):
        return Fraccion(izq.numerador*dcha.denominador + dcha.numerador*izq.denominador)

    def suma(self, otro):
        self.numerador= (self.numerador * otro.denominador) + (self.denominador * otro.numerador)
        self.denominador = self.denominador *otro.denominador 
        return self

    def resta(self,otro):
        self.numerador = self.numerador * otro.denominador - self.denominador * otro.denominador
        self.denominador = self.denominador * otro.denominador
        return self


    def division(self, otro):
        self.numerador = self.numerador * otro.denominador
        self.denominador = self.denominador * otro.numerador
        return self

    def multiplicacion(self, otro):
        self.numerador =  self.numerador * otro.numerador
        self.denominador = self.denominador * otro.denominador
        return self

fraccion1 = Fraccion(2,3)
fraccion2 = Fraccion(4,6)


misuma= fraccion2.suma(fraccion1)
miresta= fraccion2.resta(fraccion1)
midiv = fraccion2.division(fraccion1)
mimul = fraccion2.multiplicacion(fraccion1)

