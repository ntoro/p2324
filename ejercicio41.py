from http.server import HTTPServer, BaseHTTPRequestHandler

HOST_ADDRESS = "212.128.254.143"
HOST_PORT = 20090

class RequestHandler(BaseHTTPRequestHandler):
    def set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self.set_response()
        self.wfile.write("GET request for {}".format(self.path).encode('utf-8'))
        amigos = [{"nombre": "Jimena", "cuenta": "jmena"}, {"nombre": 'Natalia', "cuenta": "ntoro"}]
        if self.path == '/amigos': 
            self.wfile.write("<html><body><table>".encode('utf-8')) 
            self.wfile.write("<tr><th>Nombre</th><th>Cuenta</th></tr>".encode('utf-8'))  
            for amigo in amigos:
                self.wfile.write("<tr>".encode("utf-8"))  
                self.wfile.write("<td>{}</td>".format(amigo['nombre']).encode("utf-8"))  
                self.wfile.write("<td>{}</td>".format(amigo['cuenta']).encode("utf-8"))  
                self.wfile.write("</tr>".encode("utf-8"))  
            self.wfile.write("</table></body></html>".encode('utf-8'))  


    def do_POST(self):
        self.set_response()
        self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))

def run(server_class=HTTPServer, handler_class=BaseHTTPRequestHandler):
    """ follows example shown on docs.python.org """
    server_address = (HOST_ADDRESS, HOST_PORT)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()

if __name__ == '__main__':
    run(handler_class=RequestHandler)