class Cronometro:
    def __init__(self, tiempo1, tiempo2):
        self.tiempo1 = tiempo1
        self.tiempo2 = tiempo2

    def cronometro(self):
        if self.tiempo1 < 0:
            print('ERROR: El tiempo no puede ser negativo.')
        elif self.tiempo2 <0:
            print('ERROR: El tiempo no puede ser negativo.')
        else:
            crono = self.tiempo2 - self.tiempo1
            print(crono)

crono1 = Cronometro(5.26, 86)
crono1.cronometro()           