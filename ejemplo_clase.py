class Empleado:
    """Un ejemplo de clase para Empleados"""
    def __init__(self, n, s):
        self.nombre = n
        self.nomina = s

    def calculo_impuestos(self):
        impuestos = self.nomina*0.30
        return impuestos
    def imprime(self):
        tmp = self.calculo_impuestos()
        print ("El empleado {name} debe pagar {tax :.2f}".format(name=self.nombre,tax=tmp)) # 2f hace referencia a los decimales, si tiene nombe es porque se refiere a ese valor

empleadoPepe = Empleado( "Pepe", 20000)
empleadaAna = Empleado( "Ana",30000)

total = empleadoPepe.calculo_impuestos() + empleadaAna.calculo_impuestos()

empleadoPepe.imprime()
empleadaAna.imprime()

print("Los impuestos a pagar en total son { :.2f} euros".format(total))