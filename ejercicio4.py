class Libro:
    def __init__(self, titulo= '', autor='', prestado = 'No'):
        self.titulo = titulo
        self.autor = autor
        self.prestado = prestado

    def libroPrestado(self):
        if self.prestado == 'No':
            print('El libro no está en préstamo')
        else:
            print('El libro está en préstamo')

    def getNombre(self):
        return self.titulo
    def setNombre(self, titulo):
        self.titulo = titulo
    
    def getAutor(self):
        return self.autor
    def setAutor(self, autor):
        self.autor = autor

    def getPrestado(self):
        return self.prestado
    def setPrestado(self, prestado):
        self.prestado = prestado

libro01= Libro('Harry Potter y la piedra filosofal', 'JK Rowling', 'Si')
libro01.libroPrestado()
print(libro01.getNombre())
print(libro01.getAutor())
print(libro01.getPrestado())
    