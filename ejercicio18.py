class Vehiculo:
    def __init__(self, pasajeros, tripulacion, ruedas, matriculacion, medio):
        self.pasajeros = pasajeros
        self.tripulacion = tripulacion
        self.ruedas = ruedas
        self.matriculacion = matriculacion
        self.medio = medio
    def __str__(self):
        return f"Vehículo - Pasajeros: {self.pasajeros}, Tripulación: {self.tripulacion}, Ruedas: {self.ruedas}, Matriculación: {self.matriculacion}, Medio: {self.medio}"


    def permisoCirculacion(self, vehiculo):
        if (2024 - self.matriculacion) > 20:
            print(f'El vehículo {vehiculo} no puede circular.')

class TipoVehiculo(Vehiculo):
    def __init__(self, pasajeros, tripulacion, ruedas, matriculacion, medio, altura, puertas):
        super.__init__(pasajeros, tripulacion, ruedas, matriculacion, medio)
        self.altura = altura
        self.puertas = puertas




coche = Vehiculo(5, 0, 4, 2015, 'Tierra')
avion = Vehiculo(150, 8, 4, 2019, 'Aire')
barco = Vehiculo(100, 15, 0, 2013, 'Agua')
moto = Vehiculo(1, 0, 2, 2001, 'Tierra')
lista_vehiculos = [coche, avion, barco, moto]
for vehiculo in lista_vehiculos:
    print(vehiculo)
    vehiculo.permisoCirculacion(vehiculo)


