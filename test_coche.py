from coche_clases import Coche
import unittest


class CocheTestCase(unittest.TestCase):
    def test_aceleracion(self):
        coche1 = Coche('negro', 'Mercedes', 'GLA', '0246 INP', 40)
        aceleracion = coche1.aceleracion()
        self.assertEqual(aceleracion, 400)

    def test_freno(self):
        coche1 = Coche('negro', 'Mercedes', 'GLA', '0246 INP', 40)
        freno = coche1.freno()
        self.assertEqual(freno, 4)
        
unittest.main
