class Empleado:
    def __init__(self, nombre, salario, tasa, edad, antiguedad):
        self.__nombre = nombre
        self.__salario = salario
        self.__tasa = tasa
        self.__edad = edad
        self.__antiguedad =  antiguedad

    def CalculoImpuestos(self):
        self.__impuestos = self.__salario*self.__tasa
        print ("El empleado {name} debe pagar {tax:.2f}".format(name=self.__nombre,tax=self.__impuestos))
        return self.__impuestos

    def ImprimirEdad(self):
        print('Edad de ' + self.__nombre + ':' + str(self.__edad))

    def AhorroImpuestos(self):
        if self.__antiguedad < 1:
            print('El empleado ' + self.__nombre + 'llevo menos de un año en la empresa. No hay ahorro.')
        else:
            print('El empleado ' + self.__nombre + 'llevo más de un año en la empresa. Se ahorran un 10%.')

def displayCost(total):
    print("Los impuestos a pagar en total son {:.2f} euros".format(total))

emp1 = Empleado("Pepe", 20000, 0.35, 50, 25)
emp2 = Empleado("Ana", 30000, 0.30, 40, 15)

empleados = [emp1, emp2, Empleado("Luis", 10000, 0.10, 20, 0.5), Empleado("Luisa", 25000, 0.15, 30, 1)]
total = 0
for emp in empleados:
    emp.ImprimirEdad()
    emp.AhorroImpuestos()
    total += emp.CalculoImpuestos()
    
displayCost(total)