class Persona():
    def __init__(self, nombre, edad):
        self.nombre = nombre
        self.edad = edad

    def mostrarDatos(self):
        print('Nombre:', self.nombre)
        print('Edad:', self.edad)

    def mayorEdad(self):
        if self.edad >= 18:
            print(self.nombre, 'es mayor de edad.')


persona1 = Persona('Natalia', 18)
persona1.mostrarDatos()
persona1.mayorEdad()
